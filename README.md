# SACS

This is an automated system to grade exams based on the images of the response sheets.

Basically It takes the images of the response sheets from those who took the test and automatically identifies the painted responses and compare to the right answers.

After that, It counts how many hits (we have four areas of knowledge in this exame) in each area the student got and calculates the score based on the standard deviation from the mean

Finally, It exports a EXCEL sheet with the results and names.


1 - You need to use only the response mirror (example attached)
2- You need to provide the excel sheet with the right answers (example attached)


This is just a first attempt. I intend to improve the code, as well as, pass It to Python3 and deploy It using Kivy. This code was written in dec, 2017 and from there to here I've been studying and 
improving my skills as a programmer (I just have It as a hobby). The purpose of this profile is to track my evolution over the years.

It is written in matlab code. 


Att.


Bruno Carlos Vieira dos Santos

